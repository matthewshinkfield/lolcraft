package me.mattshinkfield.league.champions;

import java.util.Arrays;

import com.google.gson.GsonBuilder;

import me.mattshinkfield.league.abilities.Ability;

public class Champion {

	private String name;
	private double health;
	private double healthGrowth;
	private double healthRegen;
	private double healthRegenGrowth;
	private double mana;
	private double manaGrowth;	
	private double manaRegen;
	private double manaRegenGrowth;
	private double attackDamage;	
	private double attackDamageGrowth;
	private double attackSpeed;	
	private double attackSpeedGrowth;
	private double armour;	
	private double armourGrowth;	
	private double magicResistance;	
	private double magicResistanceGrowth;	
	private int movementSpeed;	
	private int range;		
	//Ability
	private Ability[] abilities;
	

	
	public Champion() {
		super();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 * @return Champion for building
	 */
	public Champion name(String name) {
		this.name = name;
		return this;
	}

	


	public Champion(String name, double health, double healthGrowth, double healthRegen, double healthRegenGrowth,
			double mana, double manaGrowth, double manaRegen, double manaRegenGrowth, double attackDamage,
			double attackDamageGrowth, double attackSpeed, double attackSpeedGrowth, double armour, double armourGrowth,
			double magicResistance, double magicResistanceGrowth, int movementSpeed, int range, Ability[] abilities) {
		super();
		this.name = name;
		this.health = health;
		this.healthGrowth = healthGrowth;
		this.healthRegen = healthRegen;
		this.healthRegenGrowth = healthRegenGrowth;
		this.mana = mana;
		this.manaGrowth = manaGrowth;
		this.manaRegen = manaRegen;
		this.manaRegenGrowth = manaRegenGrowth;
		this.attackDamage = attackDamage;
		this.attackDamageGrowth = attackDamageGrowth;
		this.attackSpeed = attackSpeed;
		this.attackSpeedGrowth = attackSpeedGrowth;
		this.armour = armour;
		this.armourGrowth = armourGrowth;
		this.magicResistance = magicResistance;
		this.magicResistanceGrowth = magicResistanceGrowth;
		this.movementSpeed = movementSpeed;
		this.range = range;
		this.abilities = abilities;
	}

	/**
	 * @return the health
	 */
	public double getHealth() {
		return health;
	}



	/**
	 * @param health the health to set
	 * @return Champion for building
	 */
	public Champion health(double health) {
		this.health = health;
		return this;
	}



	/**
	 * @return the healthGrowth
	 */
	public double getHealthGrowth() {
		return healthGrowth;
	}



	/**
	 * @param healthGrowth the healthGrowth to set
	 * @return Champion for building
	 */
	public Champion healthGrowth(double healthGrowth) {
		this.healthGrowth = healthGrowth;
		return this;
	}



	/**
	 * @return the healthRegen
	 */
	public double getHealthRegen() {
		return healthRegen;
	}



	/**
	 * @param healthRegen the healthRegen to set
	 * @return Champion for building
	 */
	public Champion healthRegen(double healthRegen) {
		this.healthRegen = healthRegen;
		return this;
	}



	/**
	 * @return the healthRegenGrowth
	 */
	public double getHealthRegenGrowth() {
		return healthRegenGrowth;
	}



	/**
	 * @param healthRegenGrowth the healthRegenGrowth to set
	 * @return Champion for building
	 */
	public Champion healthRegenGrowth(double healthRegenGrowth) {
		this.healthRegenGrowth = healthRegenGrowth;
		return this;
	}



	/**
	 * @return the mana
	 */
	public double getMana() {
		return mana;
	}



	/**
	 * @param mana the mana to set
	 * @return Champion for building
	 */
	public Champion mana(double mana) {
		this.mana = mana;
		return this;
	}



	/**
	 * @return the manaGrowth
	 */
	public double getManaGrowth() {
		return manaGrowth;
	}



	/**
	 * @param manaGrowth the manaGrowth to set
	 * @return Champion for building
	 */
	public Champion manaGrowth(double manaGrowth) {
		this.manaGrowth = manaGrowth;
		return this;
	}



	/**
	 * @return the manaRegen
	 */
	public double getManaRegen() {
		return manaRegen;
	}



	/**
	 * @param manaRegen the manaRegen to set
	 * @return Champion for building
	 */
	public Champion manaRegen(double manaRegen) {
		this.manaRegen = manaRegen;
		return this;
	}



	/**
	 * @return the manaRegenGrowth
	 */
	public double getManaRegenGrowth() {
		return manaRegenGrowth;
	}



	/**
	 * @param manaRegenGrowth the manaRegenGrowth to set
	 * @return Champion for building
	 */
	public Champion manaRegenGrowth(double manaRegenGrowth) {
		this.manaRegenGrowth = manaRegenGrowth;
		return this;
	}



	/**
	 * @return the attackDamage
	 */
	public double getAttackDamage() {
		return attackDamage;
	}



	/**
	 * @param attackDamage the attackDamage to set
	 * @return Champion for building
	 */
	public Champion attackDamage(double attackDamage) {
		this.attackDamage = attackDamage;
		return this;
	}



	/**
	 * @return the attackDamageGrowth
	 */
	public double getAttackDamageGrowth() {
		return attackDamageGrowth;
	}



	/**
	 * @param attackDamageGrowth the attackDamageGrowth to set
	 * @return Champion for building
	 */
	public Champion attackDamageGrowth(double attackDamageGrowth) {
		this.attackDamageGrowth = attackDamageGrowth;
		return this;
	}



	/**
	 * @return the attackSpeed
	 */
	public double getAttackSpeed() {
		return attackSpeed;
	}



	/**
	 * @param attackSpeed the attackSpeed to set
	 * @return Champion for building
	 */
	public Champion attackSpeed(double attackSpeed) {
		this.attackSpeed = attackSpeed;
		return this;
	}



	/**
	 * @return the attackSpeedGrowth
	 */
	public double getAttackSpeedGrowth() {
		return attackSpeedGrowth;
	}



	/**
	 * @param attackSpeedGrowth the attackSpeedGrowth to set
	 * @return Champion for building
	 */
	public Champion attackSpeedGrowth(double attackSpeedGrowth) {
		this.attackSpeedGrowth = attackSpeedGrowth;
		return this;
	}



	/**
	 * @return the armour
	 */
	public double getArmour() {
		return armour;
	}



	/**
	 * @param armour the armour to set
	 * @return Champion for building
	 */
	public Champion armour(double armour) {
		this.armour = armour;
		return this;
	}



	/**
	 * @return the armourGrowth
	 */
	public double getArmourGrowth() {
		return armourGrowth;
	}



	/**
	 * @param armourGrowth the armourGrowth to set
	 * @return Champion for building
	 */
	public Champion armourGrowth(double armourGrowth) {
		this.armourGrowth = armourGrowth;
		return this;
	}



	/**
	 * @return the magicResistance
	 */
	public double getMagicResistance() {
		return magicResistance;
	}



	/**
	 * @param magicResistance the magicResistance to set
	 * @return Champion for building
	 */
	public Champion magicResistance(double magicResistance) {
		this.magicResistance = magicResistance;
		return this;
	}



	/**
	 * @return the magicResistanceGrowth
	 */
	public double getMagicResistanceGrowth() {
		return magicResistanceGrowth;
	}



	/**
	 * @param magicResistanceGrowth the magicResistanceGrowth to set
	 * @return Champion for building
	 */
	public Champion magicResistanceGrowth(double magicResistanceGrowth) {
		this.magicResistanceGrowth = magicResistanceGrowth;
		return this;
	}



	/**
	 * @return the movementSpeed
	 */
	public int getMovementSpeed() {
		return movementSpeed;
	}



	/**
	 * @param movementSpeed the movementSpeed to set
	 * @return Champion for building
	 */
	public Champion movementSpeed(int movementSpeed) {
		this.movementSpeed = movementSpeed;
		return this;
	}



	/**
	 * @return the range
	 */
	public int getRange() {
		return range;
	}



	/**
	 * @param range the range to set
	 * @return Champion for building
	 */
	public Champion range(int range) {
		this.range = range;
		return this;
	}



	/**
	 * @return the abilities
	 */
	public Ability[] getAbilities() {
		return abilities;
	}



	/**
	 * @param abilities the abilities to set
	 * @return Champion for building
	 */
	public Champion abilities(Ability[] abilities) {
		this.abilities = abilities;
		return this;
	}

	/**
	 * @return the champion class
	 */
	public Champion build() {
		if (name == null)
			return null;
		if (mana <= -1)
			mana = 0;
		if (manaGrowth <= -1)
			manaGrowth = 0;
		if (manaRegen <= -1)
			manaRegen = 0;
		if (manaRegenGrowth <= -1)
			manaRegenGrowth = 0;
		if (attackDamage <= -1)
			return null;
		if (attackDamageGrowth <= -1)
			return null;
		if (attackSpeed <= -1) 
			return null;
		if (attackSpeedGrowth <= -1) 
			return null;
		if (armour <= 0)
			return null;
		if (armourGrowth <= 0)
			return null;
		if (magicResistance <= 0)
			return null;
		if (magicResistanceGrowth <= 0)
			return null;
		if (movementSpeed <= 0)
			return null;	
		if (range <= 0)
			return null;	
		if (abilities == null || abilities.length == 0)
			return null;
		return this;
	}


	@Override
	public String toString() {
		return new GsonBuilder().setPrettyPrinting().create().toJson(this);
	}
	
	
	
	
	
}
