package me.mattshinkfield.league.player;

import org.bukkit.entity.Player;

import me.mattshinkfield.league.abilities.Ability;
import me.mattshinkfield.league.champions.Champion;
import me.mattshinkfield.league.items.Item;

public class LeaguePlayer{


	private Item[] items;
	private int xp;
	private double health;
	private Champion champ;
	public LeaguePlayer(Item[] items, int xp, double health, Champion champ) {
		super();
		this.items = items;
		this.xp = xp;
		this.health = health;
		this.champ = champ;
	}
	/**
	 * @return the items
	 */
	public Item[] getItems() {
		return items;
	}
	/**
	 * @param items the items to set
	 */
	public void setItems(Item[] items) {
		this.items = items;
	}
	/**
	 * @return the xp
	 */
	public int getXp() {
		return xp;
	}
	/**
	 * @param xp the xp to set
	 */
	public void setXp(int xp) {
		this.xp = xp;
	}
	/**
	 * @return the health
	 */
	public double getHealth() {
		return health;
	}
	/**
	 * @param health the health to set
	 */
	public void setHealth(double health) {
		this.health = health;
	}
	/**
	 * @return the champ
	 */
	public Champion getChamp() {
		return champ;
	}
	/**
	 * @param champ the champ to set
	 */
	public void setChamp(Champion champ) {
		this.champ = champ;
	}

	
	
	
	
	
	
	
	
	
	
}
