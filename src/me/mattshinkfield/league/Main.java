package me.mattshinkfield.league;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerToggleSprintEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import me.mattshinkfield.league.champions.Champion;
import me.mattshinkfield.league.player.LeaguePlayer;
import net.md_5.bungee.api.ChatColor;

public class Main extends JavaPlugin implements Listener {
	
	private Map<String, BossBar> bossBarList;
	private List<Champion> champs;
	private Map<String, Champion> selectedChampion;
	private Map<String, LeaguePlayer> playerList;
	public void onEnable() {
		this.getServer().getPluginManager().registerEvents(this, this);
		bossBarList = new HashMap<String, BossBar>();
		selectedChampion = new HashMap<String, Champion>();
		playerList = new HashMap<String, LeaguePlayer>();

		File file = new File(this.getDataFolder()+"/Champions/champions.json");
		Type listType = new TypeToken<ArrayList<Champion>>(){}.getType();
		try (Reader reader = new FileReader(file)) {
		    Gson gson = new GsonBuilder().setPrettyPrinting().create();
		    champs = gson.fromJson(reader, listType);
		} catch (IOException e) {e.printStackTrace();}
		

		for(Player p : Bukkit.getOnlinePlayers()) {
			int i = new Random().nextInt(champs.size());
			Champion champ = champs.get(i);
			selectedChampion.put(p.getUniqueId().toString(), champ);
			LeaguePlayer player = new LeaguePlayer(null, 0, champ.getHealth(), champ);
			playerList.put(p.getUniqueId().toString(), player);
			bossBarList.put(p.getUniqueId().toString(), createBossBar(p));
			p.sendMessage("You have been selected to play as : " + champ.getName());
			p.sendMessage(champ.toString());
			AttributeInstance damage = p.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE);
			p.setWalkSpeed((float)(Math.round((champ.getMovementSpeed()*0.008/11.55)*100.0)/100.0));
			p.sendMessage(p.getWalkSpeed()+"");
		}
		
	}
	
	//0.008 * speed
	@EventHandler
    public void onPlayerDies(PlayerToggleSprintEvent ev){
		Player p = ev.getPlayer();
    	ev.setCancelled(true);
		p.setFoodLevel(2);
		this.getServer().getScheduler().scheduleSyncDelayedTask(this,  new Runnable() {
			
			@Override
			public void run() {
				p.setFoodLevel(10);
				
			}
		}, 120L);
		Vector dir = p.getEyeLocation().getDirection();

		p.setVelocity(new Vector(dir.getX()*5, 0, dir.getZ()*5));
    }


	 @EventHandler
	 public void joinEvent(PlayerJoinEvent e) {
		 Player p = e.getPlayer();
			int i = new Random().nextInt(champs.size());
			Champion champ = champs.get(i);
			selectedChampion.put(p.getUniqueId().toString(), champ);
			LeaguePlayer player = new LeaguePlayer(null, 0, champ.getHealth(), champ);
			playerList.put(p.getUniqueId().toString(), player);
			bossBarList.put(p.getUniqueId().toString(), createBossBar(p));
			p.sendMessage("You have been selected to play as : " + champ.getName());
			p.sendMessage(champ.toString());
			AttributeInstance damage = p.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE);
			p.setWalkSpeed((float)(Math.round((champ.getMovementSpeed()*0.008/11.55)*100.0)/100.0));
			p.sendMessage(p.getWalkSpeed()+"");
	 }
	@EventHandler
	public void itemHeld(PlayerItemHeldEvent e) {
		Player p = e.getPlayer();
		p.getInventory().setHeldItemSlot(5);
		switch(e.getNewSlot()) {
			case 0: {
				e.setCancelled(true);				
				break;
			}
			case 1: {
				e.setCancelled(true);
				break;
			}
			case 2: {
				e.setCancelled(true);
			}
			case 3: {
				e.setCancelled(true);
			}
			case 8: {
				e.setCancelled(true);
			}
		}
	}
	
//	@EventHandler
//	public void damageEvent(EntityDamageEvent e) {
//		if (!(e.getEntity() instanceof Player))
//			return;
//		Player p = (Player)e.getEntity();
//		Champion champ = selectedChampion.get(p.getUniqueId().toString());
//		BossBar bossBar = bossBarList.get(p.getUniqueId().toString());
//		health -= e.getDamage()*2;
//		if (health <= 0) {
//			if (bossbar != null) {
//				bossbar.removeAll();
//				bossbar = this.getServer().createBossBar(ChatColor.RED.toString() + "You: " + 0 + "/" + max + "❤", BarColor.RED, BarStyle.SEGMENTED_20, BarFlag.CREATE_FOG);
//				bossbar.setProgress(0);
//		//		bar.setProgress(p.getHealth());
//				bossbar.addPlayer(p);
//				p.setHealth(0);
//				health = max;
//			}			
//		} else {
//			e.setDamage(0);
//			p.setLevel(0);
////			p.setExp(0);
////			p.setLevel(-9999999);
//			if (bossbar != null) {
//				bossbar.removeAll();
//			}
//			bossbar = this.getServer().createBossBar(ChatColor.RED.toString() + "You: " + health + "/" + max + "❤", BarColor.RED, BarStyle.SEGMENTED_20, BarFlag.CREATE_FOG);
//			p.setExp((float)(health/max));
//			bossbar.setProgress((health/max));
//			bossbar.addPlayer(p);
//		}
//	}
//	
	public BossBar createBossBar(Player p){
		LeaguePlayer player = playerList.get(p.getUniqueId().toString());
		double health = player.getHealth();
		double max = player.getChamp().getHealth();
		BossBar bossbar = null;
		if (bossBarList.containsKey(p.getUniqueId().toString())) {
			bossbar = bossBarList.get(p.getUniqueId().toString());
			bossBarList.remove(p.getUniqueId().toString());
			bossbar.removeAll();
		}
		bossbar = this.getServer().createBossBar(ChatColor.RED.toString() + "You: " + health + "/" + max + "❤", BarColor.RED, BarStyle.SEGMENTED_20, BarFlag.CREATE_FOG);
//		p.setExp((float)(health/max));
		bossbar.setProgress((health/max));
		bossbar.addPlayer(p);
		return bossbar;
	}
	


	
}
