package me.mattshinkfield.league.abilities;

public class Ability {

	private String name;
	private String description;
	
	private double health;
	private double healthGrowth;
	private double healthRegen;
	private double healthRegenGrowth;
	private double mana;
	private double manaGrowth;	
	private double manaRegen;
	private double manaRegenGrowth;
	private double attackDamage;	
	private double attackDamageGrowth;
	private double attackSpeed;	
	private double attackSpeedGrowth;
	private double armour;	
	private double armourGrowth;	
	private double magicResistance;	
	private double magicResistanceGrowth;	
	private int movementSpeed;	
	private int range;	
	
	public Ability(String name) {
		super();
		this.name = name;
	}


	
	
}
