package me.mattshinkfield.league.crowdcontrol;

public enum CCResult {
	YES,
	ALLOWED,
	NO,
	BLOCKED,
	PARTIAL,
	MISS,
}
