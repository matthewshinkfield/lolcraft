package me.mattshinkfield.league.crowdcontrol;

import java.util.List;

import com.google.gson.GsonBuilder;

import me.mattshinkfield.league.abilities.Ability;

public class CrowdControl {

	
	private double blockMovement;
	private double blockAttacking;
	private double blockAbilities;
	private List<Ability> interrupts;
	private boolean interruptsAll;
	private CCResult reduction;
	private List<Ability> blockSpells;
	private boolean blocksSpellsAll;
	private List<Ability> removal;
	private double time;
	
	@Override
	public String toString() {
		return new GsonBuilder().setPrettyPrinting().create().toJson(this);

	}

}
